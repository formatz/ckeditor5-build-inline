Format-Z inline editor build for CKEditor 5 with custom plugins.

This is a fork of [official Ckeditor inline build](https://www.npmjs.com/package/@ckeditor/ckeditor5-build-inline) v11.0.1.

This build is currently for Schmidt-Immobilier REAT application.

### Custom plugins
* Add new block
* Copy block
* Insert tags
* Move block
* Remove block

## Install
```
yarn add -D ssh:git@gitlab.com:formatz/ckeditor5-build-inline.git#1.0.0
```

Replace 1.0.0 by the current TAG.
