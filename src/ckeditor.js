/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md.
 */

// The editor creator to use.
import InlineEditorBase from '@ckeditor/ckeditor5-editor-inline/src/inlineeditor';

import Enter from '@ckeditor/ckeditor5-enter/src/enter';
import ShiftEnter from '@ckeditor/ckeditor5-enter/src/shiftenter';
import Clipboard from '@ckeditor/ckeditor5-clipboard/src/clipboard';
import Typing from '@ckeditor/ckeditor5-typing/src/typing';
import Undo from '@ckeditor/ckeditor5-undo/src/undo';
import Alignment from '@ckeditor/ckeditor5-alignment/src/alignment';
import Heading from '@ckeditor/ckeditor5-heading/src/heading';
import Bold from '@ckeditor/ckeditor5-basic-styles/src/bold';
import Italic from '@ckeditor/ckeditor5-basic-styles/src/italic';
import List from '@ckeditor/ckeditor5-list/src/list';
import Table from '@ckeditor/ckeditor5-table/src/table';
import TableToolbar from '@ckeditor/ckeditor5-table/src/tabletoolbar';
import BlockQuote from '@ckeditor/ckeditor5-block-quote/src/blockquote';
import Highlight from '@ckeditor/ckeditor5-highlight/src/highlight';
import FontSize from '@ckeditor/ckeditor5-font/src/fontsize';
import InsertTags from './plugins/InsertTags';
import MoveBlock from './plugins/MoveBlock';
import AddNewBlock from './plugins/AddNewBlock';
import CopyBlock from './plugins/CopyBlock';
import RemoveBlock from './plugins/RemoveBlock';

import './styles.css';

export default class InlineEditor extends InlineEditorBase {}

// Plugins to include in the build.
InlineEditor.builtinPlugins = [
    Enter,
    ShiftEnter,
    Clipboard,
    Typing,
    Heading,
    Bold,
    Italic,
    List,
    Table,
    TableToolbar,
    BlockQuote,
    Highlight,
    FontSize,
    Undo,
    Alignment,
    InsertTags,
    MoveBlock,
    AddNewBlock,
    CopyBlock,
    RemoveBlock
];

// Editor configuration.
InlineEditor.defaultConfig = {
    highlight: {
        options: [
            {
                model: 'redPen',
                class: 'pen-red',
                title: 'Red pen',
                color: 'rgb(147, 37, 51)',
                type: 'pen'
            }
        ]
    },
    toolbar: {
        items: [
            'heading',
            '|',
            'bold',
            'italic',
            'bulletedList',
            'numberedList',
            'blockQuote',
            'insertTable',
            'alignment',
            'highlight',
            'fontSize',
            '|',
            'insertTags',
            '|',
            'undo',
            'redo',
            'moveBlockUp',
            'moveBlockDown',
            'addNewBlockUp',
            'addNewBlockDown',
            'copyBlock',
            '|',
            'removeBlock'
        ]
    },
    table: {
        contentToolbar: [
            'tableColumn',
            'tableRow',
            'mergeTableCells'
        ]
    },
    // This value must be kept in sync with the language defined in webpack.config.js.
    language: 'en'
};
