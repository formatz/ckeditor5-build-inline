import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';

import imageTrash from '../img/delete.svg';

class AddNewBlock extends Plugin {
    init() {
        const editor = this.editor;

        editor.ui.componentFactory.add('removeBlock', locale => {
            const view = new ButtonView(locale);

            view.set({
                label: 'Supprimer ce bloque',
                icon: imageTrash,
                tooltip: true
            });

            // Callback executed once the image is clicked.
            view.on('execute', () => {
                editor.fire('fzAction', {type: 'removeBlock'})
            });

            return view;
        });
    }
}

export default AddNewBlock
