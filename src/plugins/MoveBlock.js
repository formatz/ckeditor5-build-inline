import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';

import imageMoveUp from '../img/arrow_up.svg';
import imageMoveDown from '../img/arrow_down.svg';

class MoveBlock extends Plugin {
    init() {
        const editor = this.editor;

        editor.ui.componentFactory.add('moveBlockUp', locale => {
            const view = new ButtonView(locale);

            view.set({
                label: 'Déplacer ce bloque vers le haut',
                icon: imageMoveUp,
                tooltip: true
            });

            // Callback executed once the image is clicked.
            view.on('execute', () => {
                editor.fire('fzAction', {type: 'moveBlockUp'})
            });

            return view;
        });

        editor.ui.componentFactory.add('moveBlockDown', locale => {
            const view = new ButtonView(locale);

            view.set({
                label: 'Déplacer ce bloque vers le bas',
                icon: imageMoveDown,
                tooltip: true
            });

            // Callback executed once the image is clicked.
            view.on('execute', () => {
                editor.fire('fzAction', {type: 'moveBlockDown'})
            });

            return view;
        });
    }
}

export default MoveBlock
