import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import Model from '@ckeditor/ckeditor5-ui/src/model';
import {createDropdown, addListToDropdown} from '@ckeditor/ckeditor5-ui/src/dropdown/utils';
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';
import Collection from '@ckeditor/ckeditor5-utils/src/collection';

import imageAddAfter from '../img/add_after.svg';
import imageAddBefore from '../img/add_before.svg';
import titleImg from '../img/title_84.svg';
import subTitleImg from '../img/sub-title_84.svg';
import textImg from '../img/text_84.svg';
import imageImg from '../img/image_84.svg';
import imagesImg from '../img/images_84.svg';

class AddNewBlock extends Plugin {
    init() {
        const editor = this.editor;

        const itemDefinitions = new Collection();

        // TODO : if enough time add title dynamically, means only one entry for title with dynamic number for the level of the title
        // TODO : title could be rename to title-1 - careful API must be adapted
        itemDefinitions.add({
            type: 'button',
            model: new Model({
                label: 'Titre',
                icon: titleImg,
                class: 'ck-addNewBlock-icon',
                withText: true,
                value: 'title'
            })
        });
        // TODO : sub-title could be rename to title-2 - careful API must be adapted
        itemDefinitions.add({
            type: 'button',
            model: new Model({
                label: 'Titre niveau 2',
                icon: subTitleImg,
                class: 'ck-addNewBlock-icon',
                withText: true,
                value: 'sub-title'
            })
        });
        itemDefinitions.add({
            type: 'button',
            model: new Model({
                label: 'Titre niveau 3',
                icon: subTitleImg,
                class: 'ck-addNewBlock-icon',
                withText: true,
                value: 'title-3'
            })
        });
        itemDefinitions.add({
            type: 'button',
            model: new Model({
                label: 'Titre niveau 4',
                icon: subTitleImg,
                class: 'ck-addNewBlock-icon',
                withText: true,
                value: 'title-4'
            })
        });
        itemDefinitions.add({
            type: 'button',
            model: new Model({
                label: 'Text',
                icon: textImg,
                class: 'ck-addNewBlock-icon',
                withText: true,
                value: 'content'
            })
        });
        itemDefinitions.add({
            type: 'button',
            model: new Model({
                label: 'Image',
                icon: imageImg,
                class: 'ck-addNewBlock-icon',
                withText: true,
                value: 'image'
            })
        });
        itemDefinitions.add({
            type: 'button',
            model: new Model({
                label: 'Images groupées',
                icon: imagesImg,
                class: 'ck-addNewBlock-icon',
                withText: true,
                value: 'grouped-images'
            })
        });
        itemDefinitions.add({
            type: 'button',
            model: new Model({
                label: 'Saut de page',
                icon: imagesImg,
                class: 'ck-addNewBlock-icon',
                withText: true,
                value: 'page-break'
            })
        });

        editor.ui.componentFactory.add('addNewBlockUp', locale => {

            const dropdownView = createDropdown( locale );
            addListToDropdown( dropdownView, itemDefinitions );

            dropdownView.buttonView.set({
                label: 'Ajouter un nouveau bloque en dessus',
                icon: imageAddBefore,
                isOn: false,
                withText: false,
                tooltip: 'Ajouter un nouveau bloque en dessus'
            });

            dropdownView.extendTemplate( {
                attributes: {
                    class: [
                        'ck-addNewBlock-dropdown'
                    ]
                }
            });

            // Execute command when an item from the dropdown is selected.
            this.listenTo(dropdownView, 'execute', event => {
                editor.fire('fzAction', {type: `insert-${event.source.value}-up`})
            });

            return dropdownView;
        });

        editor.ui.componentFactory.add('addNewBlockDown', locale => {

            const dropdownView = createDropdown( locale );
            addListToDropdown( dropdownView, itemDefinitions );

            dropdownView.buttonView.set({
                label: 'Ajouter un nouveau bloque en dessous',
                icon: imageAddAfter,
                isOn: false,
                withText: false,
                tooltip: 'Ajouter un nouveau bloque en dessous'
            });

            dropdownView.extendTemplate( {
                attributes: {
                    class: [
                        'ck-addNewBlock-dropdown'
                    ]
                }
            });

            // Execute command when an item from the dropdown is selected.
            this.listenTo(dropdownView, 'execute', event => {
                editor.fire('fzAction', {type: `insert-${event.source.value}-down`})
            });

            return dropdownView;
        });
    }
}

export default AddNewBlock
