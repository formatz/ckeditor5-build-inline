import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import Model from '@ckeditor/ckeditor5-ui/src/model';
import {createDropdown, addListToDropdown} from '@ckeditor/ckeditor5-ui/src/dropdown/utils';
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';
import Collection from '@ckeditor/ckeditor5-utils/src/collection';

import {downcastElementToElement} from '@ckeditor/ckeditor5-engine/src/conversion/downcast-converters';
import {upcastElementToElement, upcastAttributeToAttribute} from '@ckeditor/ckeditor5-engine/src/conversion/upcast-converters';

import imageUnfold from '../img/unfold_more.svg';

const TAGELEMENT = 'tagElement';

class InsertTags extends Plugin {
    init() {
        const editor = this.editor;
        const schema = editor.model.schema;
        const conversion = editor.conversion;

        /** Create a new tag schema in the model **/
        schema.register(TAGELEMENT, {
            isObject: true,
            isBlock: true,
            allowContentOf: 'paragraph',
            allowWhere: '$text',
            allowAttributes: ['data-name', 'contenteditable']
        });

        /** Convertion MODEL to VIEW **/
        conversion.for('downcast').add(downcastElementToElement({
            model: TAGELEMENT,
            view: (element, writer) => {
                return writer.createContainerElement('tag', {
                    'contenteditable': element.getAttribute('contenteditable'),
                    'data-name': element.getAttribute('data-name')
                }, {priority: 5});
            },
            converterPriority: 'low'
        }));

        /** Convertion  VIEW to MODEL **/
        conversion.for('upcast')
            // Create TAG element
            .add(upcastElementToElement({
                view: {
                    name: 'tag',
                    attributes: {
                        contenteditable: true,
                        'data-name': true
                    }
                },
                model: (viewTag, modelWriter) => modelWriter.createElement(TAGELEMENT, {
                    contenteditable: viewTag.getAttribute('contenteditable'),
                    'data-name': viewTag.getAttribute('data-name')
                }),
                converterPriority: 'normal'
            }))
            // Add attribut conteditable to TAG model
            .add(upcastAttributeToAttribute({
                view: {
                    name: 'tag',
                    key: 'contenteditable',
                },
                model: 'contentEditable',
                converterPriority: 'normal'
            }))
            // Add attribut data-name to TAG model
            .add(upcastAttributeToAttribute({
                view: {
                    name: 'tag',
                    key: 'data-name',
                },
                model: 'dataName',
                converterPriority: 'normal'
            }));


        /** Add UI icon in toolbar and create dropdown menu **/
        editor.ui.componentFactory.add('insertTags', locale => {
            const itemDefinitions = new Collection();

            // Listen event insertTags to get tags from React component
            editor.on('insertTags', (event, tags) => {
                tags.map(tag => {
                    if (tag.value !== '' || tag.value !== null || tag.value !== undefined) {
                        const def = {
                            type: 'button',
                            model: new Model({
                                label: tag.name,
                                //class: 'test1',
                                withText: true,
                                value: tag.value,
                                tooltip: tag.value
                            })
                        };

                        itemDefinitions.add(def);
                    }
                })
            });

            const dropdownView = createDropdown(locale);
            addListToDropdown(dropdownView, itemDefinitions);

            dropdownView.buttonView.set({
                label: 'Attributs',
                icon: imageUnfold,
                isOn: false,
                withText: false,
                tooltip: 'Insérer un attributs'
            });

            /** Execute command when an item from the dropdown is selected **/
            this.listenTo(dropdownView, 'execute', event => {
                editor.model.change(writer => {
                    // Create html tag <tag contenteditable="false" data-name="label">value</tag>
                    const tag = writer.createElement(TAGELEMENT, {'contenteditable': 'false', 'data-name': event.source.label})
                    writer.insertText(event.source.value, tag);
                    const modelFragment = writer.createDocumentFragment();
                    writer.append(tag, modelFragment);

                    // Insert element in the current selection location.
                    editor.model.insertContent(modelFragment, editor.model.document.selection);
                });

                editor.editing.view.focus();
            });

            return dropdownView;
        });
    }
}

export default InsertTags
