import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';

import imageCopy from '../img/copy.svg';

class ImageBlock extends Plugin {
    init() {
        const editor = this.editor;

        editor.ui.componentFactory.add('addImage', locale => {
            const view = new ButtonView(locale);

            view.set({
                label: 'Copier ce bloque',
                icon: imageCopy,
                tooltip: true
            });

            // Callback executed once the image is clicked.
            view.on('execute', (event) => {
                editor.fire('fzAction', {type: 'copy'})
            });

            return view;
        });
    }
}

export default ImageBlock
